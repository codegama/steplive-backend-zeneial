<?php

use Illuminate\Database\Seeder;

class JitsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
    		[
		        'key' => 'is_jitsi',
		        'value' => NO
		    ],
		    [
		        'key' => 'jitsi_base_url',
		        'value' => 'jitsi-streaming.startstreaming.co'
		    ],
		]);
    }
}
